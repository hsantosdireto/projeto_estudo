import { Request, Response } from 'express'
import User from "../models/loginModel"
import jwt = require ("jsonwebtoken")
import bcrypt = require('bcrypt')
//import auth from './config/auth.json'
const secret = "0d85f8d6b853c6d261cbcd49b68bd1e0"

class loginContratos {
    async login(req: Request, res: Response){
        try {
            const { email, password } = req.body;
            const userWithEmail = await User.findOne({ where: { email } })
            .catch(err => console.log(err))
                if (!userWithEmail){
                    return res.status(400).json({ message: "Email não cadastrado!" });
                }
            const isValid = await bcrypt.compare(password, userWithEmail.password)
                if (!isValid){
                    return res.status(400).json({ message: "Email ou senha não coincidem!" });
                }
            
            /*if (userWithEmail.password !== password)
                return res.status(400).json({ message: "Email ou senha não coincidem!" });*/
            
                const jwtToken = jwt.sign({
                    id: userWithEmail.id,
                    email: userWithEmail.email,
                    password: userWithEmail.password
                    },secret, {expiresIn: '1d'} //process.env.JWT_SECRET as string
                );
                const verify = await jwt.verify(jwtToken, secret, (error, decoded) => {
                    if (error) {
                        return res.status(401).json({Error: "Token Inválido!"})
                    }
                })
                res.json({ message: "Bem vindo!", token: jwtToken });
        }catch(error) { 
            return res.status(500).json({message: "Falha ao realizar login!"})
        } 
    };
        async logout(req: Request, res: Response){
            res.status(200).json({ auth: false, jwtToken: null });
        }; 
      
}

export default new loginContratos();