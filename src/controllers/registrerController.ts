import { Request, Response } from 'express'
import User from "../models/loginModel"
import { hash } from 'bcrypt'

class registerContratos {
    async register(req: Request, res: Response){
        try {   
            const { fullname, email, password } = req.body;
            const buscaUserExist = await User.findOne({ where: {email}})
            .catch(err => console.log(err))
            if (buscaUserExist) {
                return res.status(409).json({ message: "Já existe um usuário com este email!" });
            }      
            const hashedPassword = await hash(password, 8);
            const newUser = await User.create({
                fullname,
                email,
                password: hashedPassword
            }) 
            res.json({ message: "Obrigado por se registrar!" ,user: newUser});
        }catch(error) {
            return res.status(500).json({message: "Não é possível registrar o usuário no momento!"})
        }  
      };
}

export default new registerContratos();