import { Request, Response } from 'express'
import ContratoInstance  from '../models/model'

class listContratos {
    async show(req: Request, res: Response)/*: Promise<Response>*/{
      try{
        const {limit,page} = req.params;
        const offset = Number(limit) * (Number(page));

        const contratosList = await ContratoInstance.findAndCountAll({
          limit: Number(limit),
          offset,
          order: ['id']
        });

        if(contratosList.count === 0){
          return res.status(404).json({message: "Sem contratos na plataforma"});
        }

        return res.json(contratosList);
      }catch(error) {
        return res.status(500).json({error: error.message})
      }
    }

    async createContrato(req: Request, res: Response):Promise<Response> {
        try {
          const {
            numerocontrato,
            empresa,
            nomedevedor,
            statuscontrato
          } = req.body;
          const creat = await ContratoInstance.create(req.body);
          return res.json(creat);
        }catch(error){
          return res.status(500).json({message: "Falha ao criar novo contrato!"})
        }       
    }

    async buscaId(req: Request, res: Response)/*: Promise<Response>*/{
        try {
          const {id} = req.params;
          const contratosId = await ContratoInstance.findOne({where: {id}});
          if(typeof id !== 'number'){
            return res.status(500).json({message: "Id é somente no formato númerico"})
          }else{
          return res.json(contratosId);
          }
        }catch(error){
          return res.json(`Falha ao buscar id: ${req.params}`)
        }     
    }
    async update(req: Request, res: Response)/*: Promise<Response>*/{
        try {
          const {id} = req.params;
          const {
            numerocontrato,
            empresa,
            nomedevedor,
            statuscontrato
          } = req.body;
          const update = await ContratoInstance.update(req.body, {where: {id: id}});
          return res.json({post: update});
        }catch(error){
          return res.json(`Falha ao atualizar id: ${req.params}`)
        }
    }
    async delete(req: Request, res: Response)/*: Promise<Response>*/{
        try {
          const {id} = req.params;
          const deleteId = await ContratoInstance.destroy({where: {id: id}});
          return res.json(deleteId);
        }catch(error){
          return res.json(`Falha ao deletar id: ${req.params}`)
        }   
    }
}

export default new listContratos();