import express =  require("express");
import contratoRoute from "./routes/route"
import sequelize from "./database/db"
import cors = require("cors")
import loginRoute from "./routes/loginRoute"

sequelize.authenticate().then(()=>{
	console.log('Conexão com o banco de dados realizada com sucesso')
  }).catch(error => {
	console.log('Falha na conexão com o banco de dados')
  })

const app = express();

app.use(cors())

const port = 3013;

app.use(express.json()); //BodyParser pra ele aceitar json

app.use('/contratos', contratoRoute, loginRoute);

app.listen(port, () => {
	console.log('Servidor iniciado na porta: ' + port);
});