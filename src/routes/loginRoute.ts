import * as express from 'express';
import loginContratos from '../controllers/loginController'
import registerContratos from '../controllers/registrerController'


const router = express.Router();

router.post('/login',loginContratos.login);
router.post('/register',registerContratos.register);
router.get('/logout',loginContratos.logout);


export default router;