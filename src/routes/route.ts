import * as express from 'express';
import contControler from '../controllers/controller'


const router = express.Router();

router.get('/list/:limit/:page',contControler.show);
router.post('/create',contControler.createContrato);
router.get('/buscaId/:id',contControler.buscaId);
router.put('/update/:id',contControler.update);
router.delete('/deleteId/:id',contControler.delete);


export default router;