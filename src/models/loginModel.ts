import { DataTypes, Model } from 'sequelize';
import db from '../database/db';

interface loginModel extends Model {
    id: number;
    fullName: string;
    email: string;
    password: string;
}

const loginModel = db.define<loginModel>('contr', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    fullname: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    }},
	{
		tableName: 'user_contratos'
	}
);

export default loginModel;