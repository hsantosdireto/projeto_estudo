import { DataTypes, Model, UUIDV4 } from 'sequelize';
import db from '../database/db';

interface contrModel extends Model {
    id: number;
    numerocontrato: number;
    empresa: string;
    nomedevedor: string;
    statuscontrato: boolean;
}

const contrModel = db.define<contrModel>('contr', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    numerocontrato: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
    },
    empresa: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nomedevedor: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    statuscontrato: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    }},
	{
		tableName: 'contratos2'
	}
);

export default contrModel;
