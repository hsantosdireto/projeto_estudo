"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = __importDefault(require("./model"));
class listContratos {
    show(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { limit, page } = req.params;
                const offset = Number(limit) * (Number(page));
                const contratosList = yield model_1.default.findAndCountAll({
                    limit: Number(limit),
                    offset,
                    order: ['id']
                });
                if (contratosList.count === 0) {
                    return res.status(404).json({ message: "Sem contratos na plataforma" });
                }
                return res.json(contratosList);
            }
            catch (error) {
                return res.status(500).json({ error: error.message });
            }
        });
    }
    createContrato(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { numerocontrato, empresa, nomedevedor, statuscontrato } = req.body;
                const creat = yield model_1.default.create(req.body);
                return res.json(creat);
            }
            catch (error) {
                return res.status(500).json({ message: "Falha ao criar novo contrato!" });
            }
        });
    }
    buscaId(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const contratosId = yield model_1.default.findOne({ where: { id } });
                if (typeof id !== 'number') {
                    return res.status(500).json({ message: "Id é somente no formato númerico" });
                }
                else {
                    return res.json(contratosId);
                }
            }
            catch (error) {
                return res.json(`Falha ao buscar id: ${req.params}`);
            }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { numerocontrato, empresa, nomedevedor, statuscontrato } = req.body;
                const update = yield model_1.default.update(req.body, { where: { id: id } });
                return res.json({ post: update });
            }
            catch (error) {
                return res.json(`Falha ao atualizar id: ${req.params}`);
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const deleteId = yield model_1.default.destroy({ where: { id: id } });
                return res.json(deleteId);
            }
            catch (error) {
                return res.json(`Falha ao deletar id: ${req.params}`);
            }
        });
    }
}
exports.default = new listContratos();
