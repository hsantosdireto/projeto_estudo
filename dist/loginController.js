"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const loginModel_1 = __importDefault(require("./loginModel"));
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
//import auth from './config/auth.json'
const secret = "0d85f8d6b853c6d261cbcd49b68bd1e0";
class loginContratos {
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { email, password } = req.body;
                const userWithEmail = yield loginModel_1.default.findOne({ where: { email } })
                    .catch(err => console.log(err));
                if (!userWithEmail) {
                    return res.status(400).json({ message: "Email não cadastrado!" });
                }
                const isValid = yield bcrypt.compare(password, userWithEmail.password);
                if (!isValid) {
                    return res.status(400).json({ message: "Email ou senha não coincidem!" });
                }
                /*if (userWithEmail.password !== password)
                    return res.status(400).json({ message: "Email ou senha não coincidem!" });*/
                const jwtToken = jwt.sign({
                    id: userWithEmail.id,
                    email: userWithEmail.email,
                    password: userWithEmail.password
                }, secret, { expiresIn: '1d' } //process.env.JWT_SECRET as string
                );
                const verify = yield jwt.verify(jwtToken, secret, (error, decoded) => {
                    if (error) {
                        return res.status(401).json({ Error: "Token Inválido!" });
                    }
                });
                res.json({ message: "Bem vindo!", token: jwtToken });
            }
            catch (error) {
                return res.status(500).json({ message: "Falha ao realizar login!" });
            }
        });
    }
    ;
    logout(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            res.status(200).json({ auth: false, jwtToken: null });
        });
    }
    ;
}
exports.default = new loginContratos();
