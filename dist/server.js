"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const route_1 = __importDefault(require("./route"));
const db_1 = __importDefault(require("./db"));
const cors = require("cors");
const loginRoute_1 = __importDefault(require("./loginRoute"));
db_1.default.authenticate().then(() => {
    console.log('Conexão com o banco de dados realizada com sucesso');
}).catch(error => {
    console.log('Falha na conexão com o banco de dados');
});
const app = express();
app.use(cors());
const port = 3013;
app.use(express.json()); //BodyParser pra ele aceitar json
app.use('/contratos', route_1.default, loginRoute_1.default);
app.listen(port, () => {
    console.log('Servidor iniciado na porta: ' + port);
});
