"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const loginModel_1 = __importDefault(require("./loginModel"));
const bcrypt_1 = require("bcrypt");
class registerContratos {
    register(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { fullname, email, password } = req.body;
                const buscaUserExist = yield loginModel_1.default.findOne({ where: { email } })
                    .catch(err => console.log(err));
                if (buscaUserExist) {
                    return res.status(409).json({ message: "Já existe um usuário com este email!" });
                }
                const hashedPassword = yield bcrypt_1.hash(password, 8);
                const newUser = yield loginModel_1.default.create({
                    fullname,
                    email,
                    password: hashedPassword
                });
                res.json({ message: "Obrigado por se registrar!", user: newUser });
            }
            catch (error) {
                return res.status(500).json({ message: "Não é possível registrar o usuário no momento!" });
            }
        });
    }
    ;
}
exports.default = new registerContratos();
