"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_1 = __importDefault(require("./db"));
const contrModel = db_1.default.define('contr', {
    id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    numerocontrato: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        unique: true,
    },
    empresa: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    nomedevedor: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    statuscontrato: {
        type: sequelize_1.DataTypes.BOOLEAN,
        allowNull: false,
    }
}, {
    tableName: 'contratos2'
});
exports.default = contrModel;
